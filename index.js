'use strict'
require('array.prototype.flatmap').shim();

var bodyParser = require('body-parser');
const express = require('express');
const server = express()
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://35.193.87.234:9200', auth: { username: "elastic", password: "lVasgu4VXz8yjBhaa8jY" } });

exports.bulkElasticAPI = async (req, res) => {
    console.log(JSON.stringify(req.body));
    let message = req.query.message || req.body.message || 'Hello World!';
    const reqbody = req.body.data;
    const userid = req.body.userid;


    reqbody.forEach((data) => {
        data.userid = userid;
        const time = new Date(parseInt(data.t));
        data.timestamp = time.toISOString();
        delete data.t;
        data.bpm = data.v;
        delete data.v;

    });

    const body = reqbody.flatMap(doc => [{ index: { _index: 'heartmonitoring' } }, doc]);
    const { body: bulkResponse } = await client.bulk({ refresh: true, body })

    if (bulkResponse.errors) {
        const erroredDocuments = []
        bulkResponse.items.forEach((action, i) => {
            const operation = Object.keys(action)[0]
            if (action[operation].error) {
                erroredDocuments.push({
                    status: action[operation].status,
                    error: action[operation].error,
                    operation: body[i * 2],
                    document: body[i * 2 + 1]
                })
            }
        })
        console.log(erroredDocuments)
    }

    const { body: count } = await client.count({ index: 'heartmonitoring' })
    res.status(200).send(count);
};
